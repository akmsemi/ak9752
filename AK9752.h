#ifndef __AK9752_H__
#define __AK9752_H__

#include "mbed.h"


/**
 * Device driver for AK9752 by AKM Semiconductors, Inc.
 * @note AK9752 is an IR sensor with I2C interface.
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak9752_reg.h"
 * #include "ak9752.h"
 * 
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 * 
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // TBD
 *
 *     while(true) {
 *              // TBD    
 *         }
 *     }
 * @endcode
 */
class AK9752
{
public:
    /**
     * Return status.
     */
    typedef enum {
        SUCCESS,                 /**< Success */
        ERROR,                   /**< Error */
        ERROR_I2C_WRITE,         /**< I2C write error */
        ERROR_I2C_READ,          /**< I2C read error */
        ERROR_ARG_OUT_OF_BOUNDS, /**< An argument is out of bounds */
        DATA_READY,            /**< Data ready */
        NOT_DATA_READY,        /**< Data ready is not asserted. */
    } Status;
    
    /**
     * Slave address of AK9752.
     */
    typedef enum {
        SLAVE_ADDR_1 = 0x64,   /**< CAD1=0, CAD0=0 */
    } SlaveAddress;
    
    /**
     * Data sampling operation modes.
     */
    typedef enum {
        MODE_STANDBY = 0x00,        /**< MODE_STANDBY:      0x00 */
        MODE_CONTINUOUS = 0x01,     /**< MODE_CONTINUOUS:   0x01 */
        MODE_SINGLE_SHOT = 0x02,    /**< MODE_SINGLE_SHOT:  0x02 */
    } OperationMode;

    /**
     * Cut-off frequency setting for the temperature sensor filter.
     */    
    typedef enum {
        FCTMP_NOFILTER = 0x00,      /**< No Filter */
        FCTMP_2P5HZ = 0x01,         /**< Fc = 2.5 Hz */
        FCTMP_0P9HZ = 0x02,         /**< Fc = 0.9 Hz */
        FCTMP_0P45HZ = 0x03,        /**< Fc = 0.45 Hz */
        FCTMP_0P22HZ = 0x04,        /**< Fc = 0.22 Hz */
    } FcTmp;
    
    /**
     * Cut-off frequency setting for the IR sensor filter.
     */    
    typedef enum {
        FCIR_NOFILTER = 0x00,      /**< No Filter */
        FCIR_2P5HZ = 0x01,         /**< Fc = 2.5 Hz */
        FCIR_0P9HZ = 0x02,         /**< Fc = 0.9 Hz */
        FCIR_0P45HZ = 0x03,        /**< Fc = 0.45 Hz */
    } FcIr;
    
    /**
     * Status of the IR and temperature sensor threshold interrupts.
     */
    typedef struct {
        bool irh;                   /**< IR Sensor crossed high threshold. */
        bool irl;                   /**< IR Sensor crossed low threshold. */
        bool tmph;                  /**< Tempearture Sensor crossed high threshold. */
        bool tmpl;                  /**< Temperature Sensor crossed low threshold. */
        bool dr;                    /**< Data Ready. */
    } InterruptStatus;

    /**
     * Threshold level interrupt settings for the IR and temperature sensor.
     */    
    typedef struct {
        int16_t thirh;        /**< High Threshold level for IR sensor. */
        int16_t thirl;        /**< Low Threshold level of IR sensor. */
        int16_t thtmph;       /**< High Threshold level of temperature sensor. */
        int16_t thtmpl;       /**< Low Threshold level of temperature sensor. */
    } Threshold;
    
    /**
     * Sensor data: IR, temperature, and status of their interrupts.
     */
    typedef struct {
        InterruptStatus intStatus;  /**< Interrupt status object */
        int16_t ir;             /**< IR Sensor Data (pA) */
        int16_t temperature;    /**< Temperature Data (deg C) */
        bool dor;               /**< Data Overrun: 1:data skip, 0:after ST2 read*/
    } SensorData;

    /**
     * Constructor.
     *
     * @param conn Instance of I2C connection
     * @param addr Slave address of the device
     */
    AK9752();

    /**
     * Initialize AK9752 connection.
     *
     * @param conn Instance of I2C connection
     * @param addr Slave address of I2C device
     */
    void init(I2C *conn, SlaveAddress addr);

    /**
     * Checks AK9752 connection.
     *
     * @return SUCCESS if connection is established, error otherwise.
     */
    Status checkConnection();

    /**
     * Retrieve the interrupt enable/disable status.
     *
     * @param intStatus Interrupt enable status
     *
     * @return SUCCESS if the interrupt status is obtained successfully, error otherwise.
     */
    Status getInterruptEnable(InterruptStatus *intStatus);
       
    /**
     * Enable or disable interrupts.
     *
     * @param intStatus interrupt status
     *
     * @return SUCCESS if the interrupt status is set successfully, error otherwise.
     */
    Status setInterruptEnable(const InterruptStatus *intStatus);
    
    /**
     * Gets sensor operation mode: Standby, Continuous or Single-Shot.
     *
     * @param mode Pointer to the operation mode.
     * @param fc_tmp Pointer to the tempearture sensor filter setting.
     * @param fc_ir Pointer to the IR sensor filter setting.
     *
     * @return SUCCESS if operation mode is set successfully, error otherwise.
     */
    Status getOperationMode(OperationMode *mode, FcTmp *fc_tmp, FcIr *fc_ir);
       
    /**
     * Sets sensor Operation mode: Standby, Continuous or Single-Shot.
     *
     * @param mode Operation mode to be set.
     * @param fc_tmp Filter cut-off frequency setting for temperature sensor.
     * @param fc_ir Filter cut-off frequency setting for IR sensor.
     *
     * @return SUCCESS if operation mode is set successfully, error otherwise.
     */
    Status setOperationMode(OperationMode mode, FcTmp fc_tmp = FCTMP_NOFILTER, FcIr fc_ir = FCIR_NOFILTER);
       
    /**
     * Sets threshold of IR/temperature sensor.
     *
     * @param th Variable of Threshold struct containing thresholds to be set.
     *
     * @return SUCCESS if thresholds are set successfully, error otherwise.
     */
    Status setThreshold(const Threshold *th);
    
    /**
     * Gets thresholds of IR/temperature sensors.
     *
     * @param th Instance of Threshold struct to store thresholds.
     *
     * @return SUCCESS if threshold is read successfully, error otherwise.
     */
    Status getThreshold(Threshold *th);
    
    /**
     * Resets the AK9752.
     *
     * @return SUCCESS if the device is reset successfully, otherwise returns other value.
     */
    Status reset();
    
    /**
     * Retrieves all sensor data: IR & Temperature data, and ST1, ST2, & INTCAUSE 
     * registers.
     *
     * @param data Pointer to the SensorData structure object to store the read data.
     *
     * @return SUCCESS if data is obtained successfully, otherwise returns other value.
     */
    Status getSensorData(SensorData *data);
    
    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    Status isDataReady();

    /**
     * Reads indicated AK9752 register(s).
     *
     * @param registerAddress Register address to be read.
     * @param buf Buffer to store the register data.
     * @param length Length in bytes to be read.
     *
     * @return SUCCESS if data is read successfully, otherwise returns other value.
     */
    Status read(char registerAddress, char *buf, int length);
    
    /**
     * Writes data into AK9752 register(s).
     *
     * @param registerAddress Register address to be written.
     * @param buf Data to be written.
     * @param length Length in bytes to be written.
     *
     * @return SUCCESS if data is written successfully, otherwise returns other value.
     */
    Status write(char registerAddress, const char *buf, int length);
    
private:

    I2C *connection;
    SlaveAddress slaveAddress;

//    const static uint8_t IR_DATA_LEN = 7;    /**<! Data length of IR sensor. From ST1 to ST2. */
    
};

#endif // __AK9752_H__
