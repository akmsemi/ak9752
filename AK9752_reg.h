#ifndef __AK9752_REG_H__
#define __AK9752_REG_H__

/**
 * Register Addresses for AK9752
 */
#define AK9752_REG_ADDR_WIA1        0x00        // Manufacturer ID
#define AK9752_REG_ADDR_WIA2        0x01        // Product ID
#define AK9752_REG_ADDR_INFO1       0x02        // AKM Internal Info
#define AK9752_REG_ADDR_INFO2       0x03        // AKM Internal Info
#define AK9752_REG_ADDR_ST1         0x04        // Status1: D0=DRDY 
#define AK9752_REG_ADDR_INTCAUSE    0x05        // Interrupt Cause: D4:D0=IRH,IRL,TMPH,TMPL,DR
#define AK9752_REG_ADDR_IRL         0x06        // IR Data, Low Byte
#define AK9752_REG_ADDR_IRH         0x07        // IR Data, High Byte
#define AK9752_REG_ADDR_TMPL        0x08        // Temperature Data, Low Byte
#define AK9752_REG_ADDR_TMPH        0x09        // Temperature Data, High Byte
#define AK9752_REG_ADDR_ST2         0x0A        // Status2: D0=DOR
#define AK9752_REG_ADDR_THIRHL      0x0B        // IR High Threshold, Low Byte
#define AK9752_REG_ADDR_THIRHH      0x0C        // IR High Threshold, High Byte
#define AK9752_REG_ADDR_THIRLL      0x0D        // IR Low Threshold, Low Byte
#define AK9752_REG_ADDR_THIRLH      0x0E        // IR Low Threshold, High Byte
#define AK9752_REG_ADDR_THTMPHL     0x0F        // Temp High Threshold, Low Byte
#define AK9752_REG_ADDR_THTMPHH     0x10        // Temp High Threshold, High Byte
#define AK9752_REG_ADDR_THTMPLL     0x11        // Temp Low Threshold, Low Byte
#define AK9752_REG_ADDR_THTMPLH     0x12        // Temp Low Threshold, High Byte
#define AK9752_REG_ADDR_INTEN       0x13        // Interrupt Enable: D4:D0=IRHI,IRLI,TMPHI,TMPLI,DRI
#define AK9752_REG_ADDR_CNTL1       0x14        // Frequency Cutoff Control: D4:D0=FCTMP[2:0],FCIR[1:0]
#define AK9752_REG_ADDR_CNTL2       0x15        // Operating Mode Control: D1:D0=MODE[1:0]
#define AK9752_REG_ADDR_CNTL3       0x16        // Soft Reset: D0=SRST

#define AK9752_REG_VALUE_WIA1       0x48        // Manufacturer Code (AKM)
#define AK9752_REG_VALUE_WIA2       0x14        // Product Code (AK9752)

#define AK9752_ST1_MASK_DRDY        0x01

#define AK9752_VAL_SOFTWARE_RESET   0x01    /**<! Software reset value. */
#define AK9752_ST1_STATUS_FLAG_DRDY 0x01    /**<! Data Ready*/
#define AK9752_INT_STATUS_FLAG_DR   0x01    /**<! Data Read */
#define AK9752_INT_STATUS_FLAG_TMPL 0x02    /**<! Temp low threshold reached */
#define AK9752_INT_STATUS_FLAG_TMPH 0x04    /**<! Temp high threshold reached */
#define AK9752_INT_STATUS_FLAG_IRL  0x08    /**<! IR low threshold reached */
#define AK9752_INT_STATUS_FLAG_IRH  0x10    /**<! IR high threshold reached */
#define AK9752_INT_STATUS_MASK      0x1F    /**<! Mask highest 3 bits of status */
#define AK9752_ST2_STATUS_FLAG_DOR  0x01    /**<! Data overrun (data read is required) */

#define AK9752_LEN_ONE_BYTE         1       /**<! Data length of 1 byte data. */
#define AK9752_LEN_BUF_THRESHOLD    8       /**<! Data length of Threshold settings. From THIRH to THTMPL */
#define AK9752_LEN_BUF_IR_DATA      7       /**<! Data length of IR sensor data: ST1,INTCAUSE,IR(16),TMP(16),ST2. */
#define AK9752_LEN_BUF_MAX			9		/**<! Maximum size of buffer, including an extra byte for the address. */


#endif // __AK9752_REG_H__
